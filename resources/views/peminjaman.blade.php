@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('js')
    <script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
    <script>
        $("#data-siswa").DataTable();
    </script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    <a href="{{ route('peminjaman.create') }}" class="btn btn-success float-right">Pinjam Baru</a>
                    <h3>Data Peminjaman</h3>
                    <div class="table-responsive mt-5">
                        <table id="data-siswa" class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Peminjam</th>
                                    <th>Buku</th>
                                    <th>Waktu</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($peminjamans as $peminjaman)
                                    <tr>
                                        <td>{{ $peminjaman->id }}</td>
                                        <td>{{ $peminjaman->siswa->name }}</td>
                                        <td>{{ $peminjaman->buku->judul }}</td>
                                        <td>{{ date("d F Y, H:i A", strtotime($peminjaman->peminjaman)) }}</td>
                                        <td>
                                            <a href="#" class="btn btn-primary">Detail</a>
                                            <a href="#" class="btn btn-success">Ubah</a>
                                            <button class="btn btn-danger" data-toggle="modal" data-target="#deleteSiswaModal{{ $buku->id }}">Hapus</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
