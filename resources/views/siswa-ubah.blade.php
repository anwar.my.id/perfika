@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('js')

@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <a href="{{ route('siswa') }}" class="btn btn-primary">Back</a>
            <div class="card mt-4">
                <div class="card-body">
                    <h3>Ubah Siswa</h3>
                    <form method="POST" action="{{ route('siswa.update', ['id' => $siswa->id]) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') ?? $siswa->name }}" required="">
                        </div>
                        <div class="form-group">
                            <label>NIS</label>
                            <input type="text" class="form-control" name="nis" value="{{ old('nis') ?? $siswa->nis }}" required="">
                        </div>
                        <div class="form-group text-right">
                            <button class="btn btn-primary">Ubah</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
