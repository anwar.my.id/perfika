@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('js')
    <script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
    <script>
        $("#data-buku").DataTable();
    </script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <a href="{{ route('buku') }}" class="btn btn-primary">Back</a>
            <div class="card mt-4">
                <div class="card-body">
                    <h3>Tambah Buku</h3>
                    <form method="POST" action="{{ route('buku.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Judul</label>
                            <input type="text" class="form-control" name="judul" required="">
                        </div>
                        <div class="form-group">
                            <label>Jumlah Buku</label>
                            <input type="number" class="form-control" name="stok" required="">
                        </div>
                        <div class="form-group">
                            <label>Cover</label>
                            <input type="file" class="form-control" name="cover" required="">
                        </div>
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea class="form-control" name="deskripsi"></textarea>
                        </div>
                        <div class="form-group text-right">
                            <button class="btn btn-primary">Tambah</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
