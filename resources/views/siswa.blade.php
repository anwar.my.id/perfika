@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('js')
    @foreach($siswas as $siswa)
        <div class="modal fade" id="deleteSiswaModal{{ $siswa->id }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="text-center">
                            <i class="fas fa-exclamation-triangle text-danger"></i>
                            Hapus Siswa
                        </h3>
                        <p>Apakah anda yakin ingin menghapus siswa <b>{{ $siswa->name }}</b>?</p>
                        <small class="text-danger">* Seluruh data peminjaman dari siswa <b>{{ $siswa->name }}</b> juga akan terhapus</small>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <a href="{{ route('siswa.destroy', ['siswa' => $siswa->id]) }}" class="btn btn-danger">Hapus</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
    <script>
        $("#data-siswa").DataTable();
    </script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    <a href="{{ route('siswa.create') }}" class="btn btn-success float-right">Tambah Siswa</a>
                    <h3>Data Siswa</h3>
                    <div class="table-responsive mt-5">
                        <table id="data-siswa" class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Nis</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($siswas as $siswa)
                                    <tr>
                                        <td>{{ $siswa->id }}</td>
                                        <td>{{ $siswa->name }}</td>
                                        <td>{{ $siswa->nis }}</td>
                                        <td>
                                            <a href="{{ route('siswa') }}" class="btn btn-primary">Detail</a>
                                            <a href="{{ route('siswa.edit', ['siswa' => $siswa->id]) }}" class="btn btn-success">Ubah</a>
                                            <button class="btn btn-danger" data-toggle="modal" data-target="#deleteSiswaModal{{ $siswa->id }}">Hapus</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
