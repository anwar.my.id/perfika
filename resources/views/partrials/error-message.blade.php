<script>
	$(function () {
		@if (session('success'))
			toastr.success("{{ session('success') }}");
		@elseif (session('warning'))
			toastr.warning("{{ session('warning') }}");
		@elseif (session('error'))
			toastr.error("{{ session('error') }}");
		@elseif (session('info'))
			toastr.info("{{ session('info') }}");
		@endif
	});
</script>