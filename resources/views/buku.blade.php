@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('js')
    @foreach($bukus as $buku)
        <div class="modal fade" id="deleteBukuModal{{ $buku->id }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="text-center">
                            <i class="fas fa-exclamation-triangle text-danger"></i>
                            Hapus Buku
                        </h3>
                        Apakah anda yakin ingin menghapus buku <b>{{ $buku->judul }}</b>?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <a href="{{ route('buku.hapus', ['id' => $buku->id]) }}" class="btn btn-danger">Hapus</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
    <script>
        $("#data-buku").DataTable();
    </script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    <a href="{{ route('buku.tambah') }}" class="btn btn-success float-right">Tambah Buku</a>
                    <h3>Data Buku</h3>
                    <div class="table-responsive mt-5">
                        <table id="data-buku" class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Cover</th>
                                    <th>Judul</th>
                                    <th>Deskripsi</th>
                                    <th>Stok</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($bukus as $buku)
                                    <tr>
                                        <td>{{ $buku->id }}</td>
                                        <td><img height="100" src="{{ Perfika::cover($buku->cover) }}"></td>
                                        <td>{{ $buku->judul }}</td>
                                        <td style="max-width: 200px">{{ Perfika::summaryStr($buku->deskripsi, 150) }}</td>
                                        <td>{{ Perfika::stock($buku) }}/{{ $buku->stok }}</td>
                                        <td>
                                            <a href="{{ route('buku') }}" class="btn btn-primary">Detail</a>
                                            <a href="{{ route('buku.ubah', ['id' => $buku->id]) }}" class="btn btn-success">Ubah</a>
                                            <button class="btn btn-danger" data-toggle="modal" data-target="#deleteBukuModal{{ $buku->id }}">Hapus</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
