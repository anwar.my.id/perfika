@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('js')
    <script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
    <script>
        $("#data-buku").DataTable();
    </script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <a href="{{ route('buku') }}" class="btn btn-primary">Back</a>
            <div class="card mt-4">
                <div class="card-body">
                    <h3>Ubah Buku</h3>
                    <form method="POST" action="{{ route('buku.update', ['id' => $buku->id]) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label>Judul</label>
                            <input type="text" class="form-control" name="judul" value="{{ old('judul') ?? $buku->judul }}" required="">
                        </div>
                        <div class="form-group">
                            <label>Jumlah Buku</label>
                            <input type="number" class="form-control" name="stok" value="{{ old('stok') ?? $buku->stok }}" required="">
                        </div>
                        <div class="form-group">
                            <label>Cover</label>
                            <input type="file" class="form-control" name="cover">
                        </div>
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea class="form-control" name="deskripsi">{{ old('deskripsi') ?? $buku->deskripsi }}</textarea>
                        </div>
                        <div class="form-group text-right">
                            <button class="btn btn-primary">Tambah</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
