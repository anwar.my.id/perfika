@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/ajax-bootstrap-select/dist/css/ajax-bootstrap-select.min.css') }}">
@endsection

@section('js')
    <script src="{{ asset('vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('vendor/ajax-bootstrap-select/dist/js/ajax-bootstrap-select.min.js') }}"></script>

    <script>
        $(".selectpicker-user")
            .selectpicker()
            .ajaxSelectPicker({
                ajax: {
                    url: '{{ route('siswa.search.json') }}',
                    method: "get",
                    dataType: 'JSON',
                    data: function () {
                        var params = {
                            q: '@{{{q}}}'
                        };
                        return params;
                    }
                },
                preprocessData: function (data) {
                    var i, l = data.length, array = [];
                    if (l) {
                        for (i = 0; i < l; i++) {
                            array.push($.extend(true, data[i], {
                                text : data[i].text,
                                value: data[i].value
                            }));
                        }
                    }
                    return array;
                },
                preserveSelected: true
            });


        $(".selectpicker-buku")
            .selectpicker()
            .ajaxSelectPicker({
                ajax: {
                    url: '{{ route('buku.search.json') }}',
                    method: "get",
                    dataType: 'JSON',
                    data: function () {
                        var params = {
                            q: '@{{{q}}}'
                        };
                        return params;
                    }
                },
                preprocessData: function (data) {
                    var i, l = data.length, array = [];
                    if (l) {
                        for (i = 0; i < l; i++) {
                            array.push($.extend(true, data[i], {
                                text : data[i].text,
                                value: data[i].value
                            }));
                        }
                    }
                    return array;
                },
                preserveSelected: true
            });
    </script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <a href="{{ route('peminjaman') }}" class="btn btn-primary">Back</a>
            <div class="card mt-4">
                <div class="card-body">
                    <h3>Pinjam Baru</h3>
                    <form method="POST" action="{{ route('siswa.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Siswa</label>
                            <select class="selectpicker-user" data-live-search="true">
                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Buku</label>
                            <select class="selectpicker-buku" data-live-search="true">
                                
                            </select>
                        </div>
                        <div class="form-group text-right">
                            <button class="btn btn-primary">Tambah</button>
                        </div>

                        <pre>TODO:
Tanggal Peminjaman (dan pengembalian?)</pre>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
