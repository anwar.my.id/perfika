<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
        	'name' => 'Administrator',
        	'email' => 'admin@admin.com',
        	'email_verified_at' => now(),
        	'password' => Hash::make('perfikaadmin123')
        ]);
    }
}
