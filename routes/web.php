<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware('auth:web')->group(function ()
{
	Route::get('/register', 'Auth\RegisterController@register')->name('register');
	Route::prefix('dashboard')->group(function ()
	{
		Route::get('/', 'PeminjamanController@index')->name('index');
		Route::get('/buku', 'BukuController@index')->name('buku');
		Route::get('/buku/tambah', 'BukuController@create')->name('buku.tambah');
		Route::post('/buku/tambah', 'BukuController@store')->name('buku.store');
		Route::get('/buku/ubah/{id}', 'BukuController@edit')->name('buku.ubah');
		Route::put('/buku/ubah/{id}', 'BukuController@update')->name('buku.update');
		Route::get('/buku/hapus/{id}', 'BukuController@update')->name('buku.hapus');
		Route::get('/buku/search/json', 'BukuController@searchJson')->name('buku.search.json');

		Route::resource('siswa', 'SiswaController');
		Route::get('/siswa', 'SiswaController@index')->name('siswa');
		Route::get('/siswa/{siswa}/destroy', 'SiswaController@destroy')->name('siswa.destroy');
		Route::get('/siswa/search/json', 'SiswaController@searchJson')->name('siswa.search.json');

		Route::get('peminjaman', 'PeminjamanController@index')->name('peminjaman');
		Route::get('peminjaman/tambah', 'PeminjamanController@create')->name('peminjaman.create');
	});
});
