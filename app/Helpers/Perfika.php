<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;

/**
 * Perfika Helper
 */
class Perfika
{
	public static function summaryStr($text, $length = 150, $suffix = "...")
	{
		if (strlen($text) > $length) {
			$text = rtrim(substr($text, 0, $length)).$suffix;
		}

		return $text;
	}

	public static function cover($cover)
	{
		try {
			$cacheIndex = 'getCover-'.$cover;
			$size = Cache::get($cacheIndex);
			if (!$size) {
				$size = Storage::size($cover);
				Cache::put($cacheIndex, $size, now()->addDays(1));
			}
			$url = Storage::url($cover) ?? asset('img/no_cover.jpg');
		} catch (\Exception $e) {
			$url = asset('img/no_cover.jpg');
		}
		return $url;
	}

	public static function deleteCover($cover)
	{
		return Storage::delete($cover);
	}

	public static function stock($buku)
	{
		return $buku->stok;
	}
}