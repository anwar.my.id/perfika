<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;
use App\User;
use Perfika;

class BukuController extends Controller
{
    public function index()
    {
    	$bukus = Buku::all();
    	return view('buku', compact('bukus'));
    }

    public function create()
    {
    	return view('buku-tambah');
    }

    public function store(Request $request)
    {
    	$buku = new Buku;
    	$buku->judul = $request->judul;
    	$buku->deskripsi = $request->deskripsi;
    	$buku->stok = $request->stok;
		$buku->cover = '';

    	if ($request->hasFile('cover')) {
    		$buku->cover = $request->cover->store('cover');
    	}

    	if ($buku->save()) {
    		return redirect()->route('buku')->with('success', 'Berhasil menambah buku');
    	} else {
    		return redirect()->route('buku')->with('error', 'Gagal menambah buku');
    	}
    }

    public function edit($id)
    {
        $buku = Buku::find($id);
        if (!$buku) {
            return back()->with('error', 'Buku tidak ditemukan');
        }

        return view('buku-ubah', compact('buku'));
    }

    public function update(Request $request, $id)
    {
        $buku = Buku::find($id);
        $buku->judul = $request->judul;
        $buku->stok = $request->stok;
        $buku->deskripsi = $request->deskripsi;
        if ($request->hasFile('cover')) {
            Perfika::deleteCover($buku->cover);
            $buku->cover = $request->cover->store('cover');
        }

        if ($buku->save()) {
            return redirect()->route('buku')->with('success', 'Berhasil mengubah buku');
        } else {
            return redirect()->route('buku')->with('error', 'Gagal mengubah buku');
        }
    }

    public function hapus($id)
    {
    	$buku = Buku::find($id);
    	if (!$buku) {
    		return back()->with('warning', 'Buku tidak ditemukan');
    	}

    	if ($buku->delete()) {
    		return redirect()->route('buku')->with('success', 'Berhasil menghapus buku');
    	} else {
    		return redirect()->route('buku')->with('error', 'Gagal menghapus buku');
    	}
    }

    public function searchJson(Request $request)
    {
        $limit = $request->limit ?? 10;
        $return = array();

        $buku = Buku::where('judul', 'LIKE', $request->q."%")
            ->orWhere('id', 'LIKE', $request->q."%")
            ->limit($limit)
            ->get()
            ->toArray();

        $getCount = count($buku);
        if ($getCount < $limit) {
            $additional = Buku::where('judul', 'LIKE', "%".$request->q."%")
                ->orWhere('id', 'LIKE', "%".$request->q."%")
                ->limit($limit - $getCount)
                ->get()
                ->toArray();

            $buku = array_merge($buku, $additional);
        }

        foreach ($buku as $s) {
            $text = "[".$s['id']."] ".$s['judul'];
            $highlight = preg_replace("/(\p{L}*?)(".preg_quote($request->q).")(\p{L}*)/ui", "$1<b>$2</b>$3", $text);
            $return[] = [
                'text' => $text,
                'value' => $s['id'],
                'data' => [
                    'divider' => false,
                    'content' => $highlight
                ]
            ];
        }

        return response()->json($return);
    }
}
