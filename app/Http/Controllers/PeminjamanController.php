<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peminjaman;

class PeminjamanController extends Controller
{
    public function index()
    {
    	$peminjamans = Peminjaman::all();
    	return view('peminjaman', compact('peminjamans'));
    }

    public function create()
    {
    	return view('peminjaman-tambah');
    }
}
