<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Siswa;
use Perfika;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siswas = Siswa::all();
        return view('siswa', compact('siswas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('siswa-tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $siswa = new Siswa;
        $siswa->name = $request->name;
        $siswa->nis = $request->nis;

        if ($siswa->save()) {
            return redirect()->route('siswa')->with('success', 'Berhasil menambah siswa');
        } else {
            return redirect()->route('siswa')->with('error', 'Gagal menambah siswa');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $siswa = Siswa::findOrFail($id);
        return view('siswa-ubah', compact('siswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $siswa = Siswa::findOrFail($id);
        $siswa->name = $request->name;
        $siswa->nis = $request->nis;

        if ($siswa->save()) {
            return redirect()->route('siswa')->with('success', 'Berhasil mengubah siswa');
        } else {
            return back()->withInput()->with('error', 'Gagal mengubah siswa');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $siswa = Siswa::find($id);
        if (!$siswa) {
            return back()->with('error', 'Siswa tidak ditemukan');
        }

        if ($siswa->delete()) {
            return redirect()->route('siswa')->with('success', 'Berhasil menghapus siswa');
        } else {
            return redirect()->route('siswa')->with('error', 'Gagal menghapus siswa');
        }
    }

    public function searchJson(Request $request)
    {
        $limit = $request->limit ?? 10;
        $return = array();

        $siswa = Siswa::where('nis', 'LIKE', $request->q."%")
            ->orWhere('name', 'LIKE', $request->q."%")
            ->limit($limit)
            ->get()
            ->toArray();

        $getCount = count($siswa);
        if ($getCount < $limit) {
            $additional = Siswa::where('nis', 'LIKE', "%".$request->q."%")
                ->orWhere('name', 'LIKE', "%".$request->q."%")
                ->limit($limit - $getCount)
                ->get()
                ->toArray();

            $siswa = array_merge($siswa, $additional);
        }

        foreach ($siswa as $s) {
            $text = "[".$s['nis']."] ".$s['name'];
            $highlight = preg_replace("/(\p{L}*?)(".preg_quote($request->q).")(\p{L}*)/ui", "$1<b>$2</b>$3", $text);
            $return[] = [
                'text' => $text,
                'value' => $s['id'],
                'data' => [
                    'divider' => false,
                    'content' => $highlight
                ]
            ];
        }

        return response()->json($return);
    }
}
